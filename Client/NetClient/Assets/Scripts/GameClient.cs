﻿using LiteNetLib;
using LiteNetLib.Utils;
using UnityEngine;
using Events;
using Model;
using System.Collections.Generic;

public class GameClient : MonoBehaviour, INetEventListener
{
    public static GameClient Instance { get; private set; }

    [SerializeField] bool isInteractive = true;
    [SerializeField] View view;

    public NetDataWriter dataWriter;
    public NetManager netManager;
    public NetPeer serverPeer;

    // Client specific data
    public long localPlayerId;
    bool isConnected;
    float x, y;
    float tickTimer;
    float tickRate = 1f / 30f;

    World world;
    Logic logic;

    void Awake()
    {
        Instance = this;
    }

    public void Connect(string ip)
    {
        Debug.Log("Client::Connect::" + ip);

        dataWriter = new NetDataWriter();
        netManager = new NetManager(this, "net");

        world = new World();
        world.players = new Dictionary<long, Player>();

        RegisterRemoveViewEvents();

        if (netManager.Start())
        {
            netManager.Connect(ip, 8888);
            Debug.Log("Client net manager started!");

            logic = new Logic(world);

            isConnected = true;
        }
        else
        {
            Debug.LogError("Could not start client net manager!");
        }
    }

    public void Disconnect()
    {
        netManager.Stop();

        EventQueue.Trigger(new PlayerDisconnectResp(localPlayerId));
    }

    void RegisterRemoveViewEvents()
    {
        // TODO: RemoteView should listen for this
        EventQueue.Subscribe<MoveReq>(e =>
        {
            MoveReq evt = e as MoveReq;

            Debug.Log(evt.id + ", " + evt.x + ", " + evt.y + " [Client]");

            dataWriter.Reset();
            evt.Serialize(dataWriter);
            serverPeer.Send(dataWriter, SendOptions.Unreliable);
        });
    }
    void Update()
    {
        if (netManager != null)
        {
            if (netManager.IsRunning)
            {
                netManager.PollEvents();
            }
        }

        if (serverPeer == null) return;
        if (!isConnected) return;

        tickTimer += Time.deltaTime;
        if (tickTimer > tickRate)
        {
            tickTimer = 0f;

            if (x != Input.mousePosition.x && y != Input.mousePosition.y)
            {
                x = Input.mousePosition.x;
                y = Input.mousePosition.y;

                if (isInteractive) EventQueue.Trigger(new MoveReq(localPlayerId, Input.mousePosition.x, Input.mousePosition.y));
            }
        }

        EventQueue.Tick();
    }

    private void OnApplicationQuit()
    {
        if (netManager != null)
            if (netManager.IsRunning)
                netManager.Stop();
    }

    public void OnNetworkLatencyUpdate(NetPeer peer, int latency) { }

    public void OnNetworkReceive(NetPeer peer, NetDataReader reader)
    {
        if (reader.Data == null)
            return;

        //Debug.Log($"OnNetworkReceive: {reader.Data.Length}");

        // TODO: register a in a dictionary
        // Trigger response events locally
        EventTag eventTag = (EventTag)reader.PeekByte();
        if (eventTag == EventTag.PlayerRegisterResp) EventQueue.Trigger(new PlayerRegisterResp(reader));
        if (eventTag == EventTag.PlayerConnectResp) EventQueue.Trigger(new PlayerConnectResp(reader));
        if (eventTag == EventTag.PlayerDisconnectResp) EventQueue.Trigger(new PlayerDisconnectResp(reader));
        if (eventTag == EventTag.MoveResp) EventQueue.Trigger(new MoveResp(reader));
    }

    public void OnPeerConnected(NetPeer peer)
    {
        serverPeer = peer;
        Debug.Log($"OnPeerConnected: {peer.EndPoint.Host} : {peer.EndPoint.Port}");
    }

    public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
    {
        Debug.Log($"OnPeerConnected: {peer.EndPoint.Host} : {peer.EndPoint.Port} Reason: {disconnectInfo.Reason.ToString()}");
    }

    public void OnNetworkError(NetEndPoint endPoint, int socketErrorCode)
    {
        Debug.LogError($"OnNetworkError: {socketErrorCode}");
    }

    public void OnNetworkReceiveUnconnected(NetEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType)
    {
        Debug.Log($"OnNetworkReceive: {reader.Data.Length}");
    }
}
