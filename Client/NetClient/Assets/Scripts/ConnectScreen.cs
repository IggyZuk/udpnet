﻿using UnityEngine;
using UnityEngine.UI;

public class ConnectScreen : MonoBehaviour
{
    public GameClient client;
    public InputField inputField;
    public Button connectButton;
    public Button disconnectButton;

    void Start()
    {
        connectButton.onClick.AddListener(() =>
        {
            client.Connect(inputField.text);
        });

        disconnectButton.onClick.AddListener(client.Disconnect);
    }
}
