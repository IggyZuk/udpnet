﻿using Events;
using Model;
using UnityEngine;

public class Logic
{
    World world;

    public Logic(World world)
    {
        EventQueue.Subscribe<PlayerRegisterResp>(e =>
        {
            PlayerRegisterResp evt = e as PlayerRegisterResp;
            GameClient.Instance.localPlayerId = evt.id;
            Debug.Log("Local Player Id: " + GameClient.Instance.localPlayerId);

            foreach (long playerId in evt.playerIds)
            {
                if (GameClient.Instance.localPlayerId != playerId)
                {
                    EventQueue.Trigger(new PlayerConnectResp(playerId));
                }
            }
        });

        EventQueue.Subscribe<PlayerConnectResp>(e =>
        {
            PlayerConnectResp evt = e as PlayerConnectResp;

            Player player = new Player { id = evt.id };
            world.players.Add(player.id, player);

            Debug.Log("PlayerConnectResp: " + player.id);
        });

        EventQueue.Subscribe<PlayerDisconnectResp>(e =>
        {
            PlayerDisconnectResp evt = e as PlayerDisconnectResp;

            world.players.Remove(evt.id);
        });

        EventQueue.Subscribe<MoveResp>(e =>
        {
            MoveResp evt = e as MoveResp;

            world.players[evt.id].x = evt.x;
            world.players[evt.id].y = evt.y;

            Debug.Log(evt.id + ", " + evt.x + ", " + evt.y + " [Server]");
        });
    }
}
