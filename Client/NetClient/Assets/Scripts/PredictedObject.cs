﻿using UnityEngine;
using Events;

public class PredictedObject : MonoBehaviour
{
    void Start()
    {
        EventQueue.Subscribe<MoveReq>(e =>
        {
            MoveReq moveEvent = e as MoveReq;

            this.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(moveEvent.x, moveEvent.y, 10f));
        });
    }
}
