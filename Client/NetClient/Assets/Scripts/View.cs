﻿using Events;
using UnityEngine;

public class View : MonoBehaviour
{
    void Start()
    {
        EventQueue.Subscribe<PlayerConnectResp>(e =>
        {
            PlayerConnectResp evt = e as PlayerConnectResp;
            SpawnPlayerView(evt.id);
        });
    }

    public void SpawnPlayerView(long playerId)
    {
        GameObject prefab = Resources.Load("Prefabs/PlayerView") as GameObject;
        GameObject go = Instantiate(prefab);
        PlayerView player = go.GetComponent<PlayerView>();
        player.Init(playerId);
    }
}
