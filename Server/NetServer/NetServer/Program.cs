﻿using LiteNetLib;
using LiteNetLib.Utils;
using System;
using Events;
using State;
using System.Collections.Generic;
using System.Linq;
using Jitter.Collision;
using Jitter;
using Jitter.Dynamics;
using Jitter.Collision.Shapes;

namespace NetServer
{
    public class Program : INetEventListener
    {
        public static NetManager serverNetManager;
        public static NetDataWriter dataWriter;

        Model model;
        RemoteView remoteView;

        public void Run()
        {
            // Create a new model
            model = new Model();
            model.players = new Dictionary<long, Player>();

            CollisionSystem collisionSystem = new CollisionSystemSAP();
            World world = new World(collisionSystem);

            Shape shape = new SphereShape(1f);
            RigidBody body = new RigidBody(shape);
            world.AddBody(body);

            remoteView = new RemoteView();

            // Subscribe to logic events
            EventQueue.Subscribe<PlayerConnectResp>(e =>
            {
                PlayerConnectResp evt = e as PlayerConnectResp;

                Player player = new Player { id = evt.id };
                model.players.Add(player.id, player);
            });

            EventQueue.Subscribe<PlayerDisconnectResp>(e =>
            {
                PlayerDisconnectResp evt = e as PlayerDisconnectResp;

                model.players.Remove(evt.id);
            });

            EventQueue.Subscribe<MoveReq>(e =>
            {
                MoveReq moveEvt = e as MoveReq;

                EventQueue.Trigger(new MoveResp(moveEvt.id, moveEvt.x, moveEvt.y));
            });

            // Start listening for client connections
            try
            {
                dataWriter = new NetDataWriter();
                serverNetManager = new NetManager(this, 4, "net");

                if (serverNetManager.Start(8888))
                    Console.WriteLine("Server started listening on port 8888");
                else
                {
                    Console.WriteLine("Server cold not start!");
                    return;
                }

                while (serverNetManager.IsRunning)
                {
                    serverNetManager.PollEvents();

                    EventQueue.Tick();
                    world.Step(0.1f, false);

                    System.Threading.Thread.Sleep(15);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        static void Main(string[] args)
        {
            Program program = new Program();
            program.Run();

            Console.ReadKey();
        }

        public void OnPeerConnected(NetPeer peer)
        {
            Console.WriteLine($"OnPeerConnected: {peer.EndPoint.Host} : {peer.EndPoint.Port}");

            Console.WriteLine($"Peer ConnectId: {peer.ConnectId}");

            EventQueue.Trigger(new PlayerRegisterResp(peer.ConnectId, world.players.Keys.ToArray()));
            EventQueue.Trigger(new PlayerConnectResp(peer.ConnectId));
        }

        public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
        {
            Console.WriteLine($"OnPeerConnected: {peer.EndPoint.Host} : {peer.EndPoint.Port} Reason: {disconnectInfo.Reason.ToString()}");

            EventQueue.Trigger(new PlayerDisconnectResp(peer.ConnectId));
        }

        public void OnNetworkError(NetEndPoint endPoint, int socketErrorCode)
        {
            try
            {
                Console.WriteLine($"OnNetworkError: {socketErrorCode}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        public void OnNetworkReceive(NetPeer peer, NetDataReader reader)
        {
            Console.WriteLine("Received Data: " + reader.Data.Length);

            EventTag eventTag = (EventTag)reader.PeekByte();
            if (eventTag == EventTag.MoveReq) EventQueue.Trigger(new MoveReq(reader));
        }

        public void OnNetworkReceiveUnconnected(NetEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType)
        {
            try
            {
                Console.WriteLine($"OnNetworkReceiveUnconnected");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
        {
            try { }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }
    }
}
