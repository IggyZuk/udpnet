﻿using System.Linq;
using Events;
using LiteNetLib;

namespace NetServer
{
    public class RemoteView
    {
        public RemoteView()
        {
            // TODO: most of this can be generic
            // Special case: Event -> Broadcast; SpecificID

            // Subscribe to events and send them over the network
            EventQueue.Subscribe<PlayerRegisterResp>(e =>
            {
                PlayerRegisterResp evt = e as PlayerRegisterResp;

                Program.dataWriter.Reset();
                evt.Serialize(Program.dataWriter);

                Program.serverNetManager
                    .GetPeers()
                    .Single(p => p.ConnectId == evt.id)
                    .Send(Program.dataWriter, SendOptions.ReliableOrdered);
            });

            EventQueue.Subscribe<PlayerConnectResp>(e =>
            {
                PlayerConnectResp evt = e as PlayerConnectResp;

                Program.dataWriter.Reset();
                evt.Serialize(Program.dataWriter);
                Program.serverNetManager.SendToAll(Program.dataWriter, SendOptions.ReliableUnordered);
            });

            EventQueue.Subscribe<PlayerDisconnectResp>(e =>
            {
                PlayerDisconnectResp evt = e as PlayerDisconnectResp;

                Program.dataWriter.Reset();
                evt.Serialize(Program.dataWriter);
                Program.serverNetManager.SendToAll(Program.dataWriter, SendOptions.ReliableUnordered);
            });

            EventQueue.Subscribe<MoveResp>(e =>
            {
                MoveResp moveEvt = e as MoveResp;

                Program.dataWriter.Reset();
                moveEvt.Serialize(Program.dataWriter);
                Program.serverNetManager.SendToAll(Program.dataWriter, SendOptions.Unreliable);
            });
        }
    }
}
