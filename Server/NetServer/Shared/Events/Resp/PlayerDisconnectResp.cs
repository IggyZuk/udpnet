﻿using LiteNetLib.Utils;

namespace Events
{
    public class PlayerDisconnectResp : IEvent
    {
        EventTag tag = EventTag.PlayerDisconnectResp;

        public long id;

        public PlayerDisconnectResp(long id)
        {
            this.id = id;
        }

        public PlayerDisconnectResp(NetDataReader reader)
        {
            Deserialize(reader);
        }

        public void Serialize(NetDataWriter writer)
        {
            writer.Put((byte)tag);

            writer.Put(id);
        }

        public void Deserialize(NetDataReader reader)
        {
            tag = (EventTag)reader.GetByte();

            id = reader.GetLong();
        }
    }
}