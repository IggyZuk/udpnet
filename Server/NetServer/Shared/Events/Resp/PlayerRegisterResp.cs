﻿using LiteNetLib.Utils;

namespace Events
{
    public class PlayerRegisterResp : IEvent
    {
        EventTag tag = EventTag.PlayerRegisterResp;

        public long id;
        public long[] playerIds;

        public PlayerRegisterResp(long id, long[] playerIds)
        {
            this.id = id;
            this.playerIds = playerIds;
        }

        public PlayerRegisterResp(NetDataReader reader)
        {
            Deserialize(reader);
        }

        public void Serialize(NetDataWriter writer)
        {
            writer.Put((byte)tag);

            writer.Put(id);
            writer.PutArray(playerIds);
        }

        public void Deserialize(NetDataReader reader)
        {
            tag = (EventTag)reader.GetByte();

            id = reader.GetLong();
            playerIds = reader.GetLongArray();
        }
    }
}