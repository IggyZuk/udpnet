﻿using System.Collections.Generic;
using LiteNetLib.Utils;

namespace State
{
    public class Model : INetSerializable
    {
        public Dictionary<long, Player> players;

        public void Serialize(NetDataWriter writer)
        {
            foreach (Player player in players.Values)
            {
                player.Serialize(writer);
            }
        }

        public void Deserialize(NetDataReader reader)
        {
            players = new Dictionary<long, Player>();

            while (!reader.EndOfData)
            {
                Player player = new Player();
                player.Deserialize(reader);
                players.Add(player.id, player);
            }
        }
    }

    public class Player : INetSerializable
    {
        public long id;
        public float x;
        public float y;

        public void Serialize(NetDataWriter writer)
        {
            writer.Put(id);
            writer.Put(x);
            writer.Put(y);
        }

        public void Deserialize(NetDataReader reader)
        {
            id = reader.GetLong();
            x = reader.GetFloat();
            y = reader.GetFloat();
        }
    }
}