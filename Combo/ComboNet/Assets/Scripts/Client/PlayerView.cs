﻿using UnityEngine;
using Events;

public class PlayerView : MonoBehaviour
{
    public long playerId;

    public void Init(long playerId)
    {
        this.playerId = playerId;

        GameClient.Instance.eventQueue.Subscribe<MoveResp>(e =>
        {
            MoveResp evt = e as MoveResp;

            if (evt.id == playerId)
            {
                this.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(evt.x, evt.y, 10f));
            }
        });

        GameClient.Instance.eventQueue.Subscribe<PlayerDisconnectResp>(e =>
        {
            PlayerDisconnectResp evt = e as PlayerDisconnectResp;

            if (evt.id == playerId)
            {
                Destroy(this.gameObject);
            }
        });
    }
}