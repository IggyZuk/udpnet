﻿using Events;
using State;
using UnityEngine;

public class Logic
{
    Model state;

    public Logic(Model model)
    {
        GameClient.Instance.eventQueue.Subscribe<PlayerRegisterResp>(e =>
        {
            PlayerRegisterResp evt = e as PlayerRegisterResp;
            GameClient.Instance.localPlayerId = evt.id;
            Debug.Log("Local Player Id: " + GameClient.Instance.localPlayerId);

            foreach (long playerId in evt.playerIds)
            {
                if (GameClient.Instance.localPlayerId != playerId)
                {
                    GameClient.Instance.eventQueue.Trigger(new PlayerConnectResp(playerId));
                }
            }
        });

        GameClient.Instance.eventQueue.Subscribe<PlayerConnectResp>(e =>
        {
            PlayerConnectResp evt = e as PlayerConnectResp;

            Player player = new Player { id = evt.id };
            model.players.Add(player.id, player);

            Debug.Log("PlayerConnectResp: " + player.id);
        });

        GameClient.Instance.eventQueue.Subscribe<PlayerDisconnectResp>(e =>
        {
            PlayerDisconnectResp evt = e as PlayerDisconnectResp;

            model.players.Remove(evt.id);
        });

        GameClient.Instance.eventQueue.Subscribe<MoveResp>(e =>
        {
            MoveResp evt = e as MoveResp;

            model.players[evt.id].x = evt.x;
            model.players[evt.id].y = evt.y;

            Debug.Log(evt.id + ", " + evt.x + ", " + evt.y + " [Server]");
        });
    }
}
