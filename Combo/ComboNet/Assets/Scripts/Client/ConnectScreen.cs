﻿using UnityEngine;
using UnityEngine.UI;

public class ConnectScreen : MonoBehaviour
{
    public GameClient client;
    public GameServer server;

    public InputField inputField;

    public Button startServerButton;
    public Button connectButton;
    public Button disconnectButton;

    void Start()
    {
        startServerButton.onClick.AddListener(() =>
        {
            server.Run();
        });

        connectButton.onClick.AddListener(() =>
        {
            client.Connect(inputField.text);
        });

        disconnectButton.onClick.AddListener(client.Disconnect);
    }
}
