﻿using System.Linq;
using Events;
using LiteNetLib;

public class RemoteView
{
    public RemoteView()
    {
        // TODO: most of this can be generic
        // Special case: Event -> Broadcast; SpecificID

        // Subscribe to events and send them over the network
        GameServer.Instance.eventQueue.Subscribe<PlayerRegisterResp>(e =>
        {
            PlayerRegisterResp evt = e as PlayerRegisterResp;

            GameServer.Instance.dataWriter.Reset();
            evt.Serialize(GameServer.Instance.dataWriter);

            GameServer.Instance.serverNetManager
                .GetPeers()
                .Single(p => p.ConnectId == evt.id)
                .Send(GameServer.Instance.dataWriter, SendOptions.ReliableOrdered);
        });

        GameServer.Instance.eventQueue.Subscribe<PlayerConnectResp>(e =>
        {
            PlayerConnectResp evt = e as PlayerConnectResp;

            GameServer.Instance.dataWriter.Reset();
            evt.Serialize(GameServer.Instance.dataWriter);
            GameServer.Instance.serverNetManager.SendToAll(GameServer.Instance.dataWriter, SendOptions.ReliableUnordered);
        });

        GameServer.Instance.eventQueue.Subscribe<PlayerDisconnectResp>(e =>
        {
            PlayerDisconnectResp evt = e as PlayerDisconnectResp;

            GameServer.Instance.dataWriter.Reset();
            evt.Serialize(GameServer.Instance.dataWriter);
            GameServer.Instance.serverNetManager.SendToAll(GameServer.Instance.dataWriter, SendOptions.ReliableUnordered);
        });

        GameServer.Instance.eventQueue.Subscribe<MoveResp>(e =>
        {
            MoveResp moveEvt = e as MoveResp;

            GameServer.Instance.dataWriter.Reset();
            moveEvt.Serialize(GameServer.Instance.dataWriter);
            GameServer.Instance.serverNetManager.SendToAll(GameServer.Instance.dataWriter, SendOptions.Unreliable);
        });
    }
}
