﻿using LiteNetLib;
using LiteNetLib.Utils;
using System;
using Events;
using State;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameServer : MonoBehaviour, INetEventListener
{
    public static GameServer Instance { get; private set; }

    public EventQueue eventQueue = new EventQueue();

    public NetManager serverNetManager;
    public NetDataWriter dataWriter;

    public bool isRunning;

    Model model;
    RemoteView remoteView;

    void Awake()
    {
        Instance = this;
    }

    void OnDrawGizmos()
    {
        if (!Application.isPlaying) return;
        if (!isRunning) return;

        model.players.Values
             .ToList()
             .ForEach(p => Gizmos.DrawWireSphere(Camera.main.ScreenToWorldPoint(new Vector3(p.x, p.y, 10)), 1f));
    }

    public void Run()
    {
        isRunning = true;

        // Create a new model
        model = new Model();
        model.players = new Dictionary<long, Player>();

        remoteView = new RemoteView();

        // Subscribe to logic events
        eventQueue.Subscribe<PlayerConnectResp>(e =>
        {
            PlayerConnectResp evt = e as PlayerConnectResp;

            Player player = new Player { id = evt.id };
            model.players.Add(player.id, player);
        });

        eventQueue.Subscribe<PlayerDisconnectResp>(e =>
        {
            PlayerDisconnectResp evt = e as PlayerDisconnectResp;

            model.players.Remove(evt.id);
        });

        eventQueue.Subscribe<MoveReq>(e =>
        {
            MoveReq moveEvt = e as MoveReq;

            eventQueue.Trigger(new MoveResp(moveEvt.id, moveEvt.x, moveEvt.y));
        });

        eventQueue.Subscribe<MoveResp>(e =>
        {
            MoveResp evt = e as MoveResp;

            model.players[evt.id].x = evt.x;
            model.players[evt.id].y = evt.y;
        });

        // Start listening for client connections
        try
        {
            dataWriter = new NetDataWriter();
            serverNetManager = new NetManager(this, 4, "net");

            if (serverNetManager.Start(8888))
                Debug.Log("Server started listening on port 8888");
            else
            {
                Debug.Log("Server cold not start!");
                return;
            }
        }
        catch (Exception ex)
        {
            Debug.Log($"Error: {ex.Message}");
        }
    }

    void FixedUpdate()
    {
        if (!isRunning) return;

        if (serverNetManager.IsRunning)
        {
            serverNetManager.PollEvents();

            eventQueue.Tick();
        }
    }

    public void OnPeerConnected(NetPeer peer)
    {
        Debug.Log($"OnPeerConnected: {peer.EndPoint.Host} : {peer.EndPoint.Port}");

        Debug.Log($"Peer ConnectId: {peer.ConnectId}");

        eventQueue.Trigger(new PlayerRegisterResp(peer.ConnectId, model.players.Keys.ToArray()));
        eventQueue.Trigger(new PlayerConnectResp(peer.ConnectId));
    }

    public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
    {
        Debug.Log($"OnPeerConnected: {peer.EndPoint.Host} : {peer.EndPoint.Port} Reason: {disconnectInfo.Reason.ToString()}");

        eventQueue.Trigger(new PlayerDisconnectResp(peer.ConnectId));
    }

    public void OnNetworkError(NetEndPoint endPoint, int socketErrorCode)
    {
        try
        {
            Debug.Log($"OnNetworkError: {socketErrorCode}");
        }
        catch (Exception ex)
        {
            Debug.Log($"Error: {ex.Message}");
        }
    }

    public void OnNetworkReceive(NetPeer peer, NetDataReader reader)
    {
        Debug.Log("Received Data: " + reader.Data.Length);

        EventTag eventTag = (EventTag)reader.PeekByte();
        if (eventTag == EventTag.MoveReq) eventQueue.Trigger(new MoveReq(reader));
    }

    public void OnNetworkReceiveUnconnected(NetEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType)
    {
        try
        {
            Debug.Log($"OnNetworkReceiveUnconnected");
        }
        catch (Exception ex)
        {
            Debug.Log($"Error: {ex.Message}");
        }
    }

    public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
    {
        try { }
        catch (Exception ex)
        {
            Debug.Log($"Error: {ex.Message}");
        }
    }
}
