﻿using LiteNetLib.Utils;

namespace Events
{
    public class MoveResp : IEvent
    {
        EventTag tag = EventTag.MoveResp;

        public long id;
        public float x;
        public float y;

        public MoveResp(long id, float x, float y)
        {
            this.id = id;
            this.x = x;
            this.y = y;
        }

        public MoveResp(NetDataReader reader)
        {
            Deserialize(reader);
        }

        public void Serialize(NetDataWriter writer)
        {
            writer.Put((byte)tag);

            writer.Put(id);
            writer.Put(x);
            writer.Put(y);
        }

        public void Deserialize(NetDataReader reader)
        {
            tag = (EventTag)reader.GetByte();

            id = reader.GetLong();
            x = reader.GetFloat();
            y = reader.GetFloat();
        }
    }
}