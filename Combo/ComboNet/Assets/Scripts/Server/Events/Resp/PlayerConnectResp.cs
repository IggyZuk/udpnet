﻿using LiteNetLib.Utils;

namespace Events
{
    public class PlayerConnectResp : IEvent
    {
        EventTag tag = EventTag.PlayerConnectResp;

        public long id;

        public PlayerConnectResp(long id)
        {
            this.id = id;
        }

        public PlayerConnectResp(NetDataReader reader)
        {
            Deserialize(reader);
        }

        public void Serialize(NetDataWriter writer)
        {
            writer.Put((byte)tag);

            writer.Put(id);
        }

        public void Deserialize(NetDataReader reader)
        {
            tag = (EventTag)reader.GetByte();

            id = reader.GetLong();
        }
    }
}