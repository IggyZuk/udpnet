﻿using System.Collections.Generic;
using System;

namespace Events
{
    public class EventQueue
    {
        readonly Queue<IEvent> events = new Queue<IEvent>();
        readonly Dictionary<Type, List<Action<IEvent>>> subscribers = new Dictionary<Type, List<Action<IEvent>>>();

        public void Trigger<T>(T evt) where T : IEvent
        {
            events.Enqueue(evt);
        }

        public void Subscribe<T>(Action<IEvent> callback) where T : IEvent
        {
            Type eventType = typeof(T);
            if (!subscribers.ContainsKey(eventType))
            {
                subscribers.Add(eventType, new List<Action<IEvent>>());
            }

            subscribers[eventType].Add(callback);
        }

        public void Tick()
        {
            while (events.Count > 0)
            {
                IEvent evt = events.Dequeue();

                Console.WriteLine($"Event: {evt.GetType().Name}");

                subscribers[evt.GetType()].ForEach(x => x?.Invoke(evt));
            }
        }
    }
}