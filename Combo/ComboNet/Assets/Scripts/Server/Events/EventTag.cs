﻿namespace Events
{
    public enum EventTag : byte
    {
        PlayerRegisterResp,
        PlayerConnectResp,
        PlayerDisconnectResp,
        MoveReq,
        MoveResp,
    }
}