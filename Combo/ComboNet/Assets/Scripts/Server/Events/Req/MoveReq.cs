﻿using LiteNetLib.Utils;

namespace Events
{
    public class MoveReq : IEvent
    {
        EventTag tag = EventTag.MoveReq;

        public long id;
        public float x;
        public float y;

        public MoveReq(long id, float x, float y)
        {
            this.id = id;
            this.x = x;
            this.y = y;
        }

        public MoveReq(NetDataReader reader)
        {
            Deserialize(reader);
        }

        public void Serialize(NetDataWriter writer)
        {
            writer.Put((byte)tag);

            writer.Put(id);
            writer.Put(x);
            writer.Put(y);
        }

        public void Deserialize(NetDataReader reader)
        {
            tag = (EventTag)reader.GetByte();

            id = reader.GetLong();
            x = reader.GetFloat();
            y = reader.GetFloat();
        }
    }
}